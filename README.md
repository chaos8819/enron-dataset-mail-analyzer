# ENRON DATASET EMAIL ANALYZER

This project analyzes the enron dataset in order to identify the average length in word of the email and the top 100 email recepients in the dataset.

## Main Assumption

1. The enron dataset data in xml format will be processed.
2. In order to identify the average of the emails, the text_000 folder of each zip file will be processed.
3. only the .txt files classified in the xml with 'DocType="Message"' will be taken into account for the computation, since the other files are the text of the attachments.
4. no preprocessing of the texts will be computed.
5. in the dataset, two kind of email format are been indentified, the classical mail format and a particular one whose pattern is /O=ENRON/OU=NA/CN=RECIPIENTS/CN=<name>. Since the last one is very common, both the two format will be processed via regex.
6. after an analysis of the data and considering that for most of the service provider the email is case insensitive, they will be transformed in lower case.

## Installation Instruction

1. create a ec2 instance of type t2.2xlarge.
2. attach to it a EBS with 400 GB of space and containing the enron_dataset.
3. launch the instance.
4. install Java 8.
5. install Python 3.5.2.
6. download Spark executing the command 'wget http://d3kbcqa49mib13.cloudfront.net/spark-2.1.0-bin-hadoop2.7.tgz'.
7. install Spark from the downloded distribution.
8. from command line create the directory '/enron' and launch the command 'sudo chmod 777 /enron'.
9. launch the command 'sudo mount volume_name /enron' where volume_name is the name of the volume which contains the enron dataset
10. launch the command 'spark-submit enron_email_analyzer_v2.py path' where path is the absolute path to the folder where the .zip files are located