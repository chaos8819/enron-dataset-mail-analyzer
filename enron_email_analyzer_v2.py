import sys
import zipfile
import os
import re

import xml.etree.ElementTree as ET
from pyspark import SparkContext
from pyspark import StorageLevel


XML_FILE_NAME = "enron_v2.xml"  #to modify
EMAILS_TEXT_DIR = "text_dir"	#to modify
EMAILS_XML_DIR = "xml_dir"
EXTRACT_DIR = "/enron/enron_extracted_version"

def extract_from_zip(path):

	zip_path = path.join(path)

	zip_list = [filename for filename in os.listdir(path) if filename.endswith('xml.zip')]

	for filename in zip_list:
		with zipfile.ZipFile(os.path.join(path,filename), 'r') as zip:
			files = zip.namelist()
			for f in files:
				if f.startswith('text_000/') and f.endswith('.txt'):
					zip.extract(f, os.path.join(EXTRACT_DIR, EMAILS_TEXT_DIR))
				if not f.startswith('text_000') and not f.startswith('native_000') and f.endswith('.xml'):
					zip.extract(f, os.path.join(EXTRACT_DIR, EMAILS_XML_DIR))


def getEmails():
	to_emails = []
	cc_emails = []
	docs_set = set()

	xml_list = [xml for xml in os.listdir(os.path.join(EXTRACT_DIR, EMAILS_XML_DIR))]

	for xml_file in xml_list:
		tree = ET.parse(os.path.join(EXTRACT_DIR, EMAILS_XML_DIR, xml_file))
		root = tree.getroot()
		for document in root.findall('.//Tags'):
			for tag in document.findall('.//Tag'):
				attr = tag.attrib
				if attr['TagName'] == '#To':
					to_attrib_val = attr['TagValue']
					to_emails.extend(re.findall(r'[\w\.]+[\w]+@[\w\.]+[\w]+', to_attrib_val))
					to_emails.extend(re.findall(r'/O=ENRON/OU=NA/CN=RECIPIENTS/CN=[\w]+', to_attrib_val))
				if attr['TagName'] == '#CC':
					cc_attrib_val = attr['TagValue']
					cc_emails.extend(re.findall(r'[\w\.]+[\w]+@[\w\.]+[\w]+', cc_attrib_val))	
					cc_emails.extend(re.findall(r'/O=ENRON/OU=NA/CN=RECIPIENTS/CN=[\w]+', cc_attrib_val))

		for document in root.findall('.//Document'):
			doc_attr = document.attrib
			if doc_attr['DocType'] == 'Message':
				for f in document.findall(".//*[@FileType='Text']/ExternalFile"):
					file_attr = f.attrib
					docs_set = docs_set | set([os.path.join(EXTRACT_DIR, EMAILS_TEXT_DIR, 'text_000/', file_attr['FileName'])])

	return (to_emails, cc_emails, docs_set)

if __name__ == '__main__':

	if len(sys.argv) < 2:
		print("Usage: EnronEmailAnalyzer <path>")
		exit(-1)

	sc = SparkContext()

	path = sys.argv[1]

	extract_from_zip(path)

	to_emails, cc_emails, docs_set = getEmails()

	#starting spark computation

	#Compute average
	raw_email_texts = sc.wholeTextFiles(os.path.join(EXTRACT_DIR, EMAILS_TEXT_DIR, 'text_000/'))
	filtered_email_text = raw_email_texts.filter(lambda x: x[0].split(':')[1] in docs_set)
	filtered_email_text.persist(StorageLevel.MEMORY_AND_DISK)
	word_count = filtered_email_text.flatMap(lambda x: x[1].split(" ")).count()
	email_count = len(docs_set)

	average = word_count / email_count

	#Compute top 100 email recipients
	rdd_to_emails = sc.parallelize(to_emails)
	rdd_cc_emails = sc.parallelize(cc_emails)

	mapped_to_emails = rdd_to_emails.map(lambda x: x.lower()).map(lambda x: (x, 1))
	mapped_cc_emails = rdd_cc_emails.map(lambda x: x.lower()).map(lambda x: (x, 0.5))

	top_100_emails = mapped_to_emails.union(mapped_cc_emails) \
	.reduceByKey(lambda x, y: x + y) \
	.map(lambda x: (x[1], x[0])) \
	.sortByKey(ascending=False) \
	.take(100)
	

	#print results
	print("Average email size: " + str(average))
	print("Top 100 recipients email address:")
	for k, v in top_100_emails:
		print(str(v))



